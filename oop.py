class Employee:
    numb_of_empls = 0
    raise_amt = 1.04

    def __init__(self, first, last, pay) -> None:
        self.first = first
        self.last = last
        self.email = str.lower(first + "." + last + "@company.com")
        self.pay = pay

        Employee.numb_of_empls += 1

    def fullname(self):
        return f"{self.first} {self.last}"

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amt)

    @classmethod
    def self_raise_amt(cls, amount):
        cls.raise_amt = amount


emp_1 = Employee("Corey", "Schafer", 50)

print(Employee.raise_amt)

Employee.self_raise_amt(1.10)

emp_2 = Employee("Test", "Employee", 60)

print(Employee.raise_amt)
