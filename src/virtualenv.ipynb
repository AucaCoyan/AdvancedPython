{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using Virtual Environments\n",
    "\n",
    "- Difference between virtual environments (virtualenv or venv), containers, and virtual machines\n",
    "- Creating a virtual environment\n",
    "- Activating and deactivating virtualenvs\n",
    "- Installing packages in a virtual environment\n",
    "- Using requirements.txt files for reproducability\n",
    "- Using wheels for faster installations\n",
    "- Cleaning up virtualenvs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Virtual Machines, Containers, and Virtualenvs\n",
    "\n",
    "## Virtual Machine\n",
    "\n",
    "- Isolated image (\"guest\") of a computer running its own operating system\n",
    "- Can have different OS than the host\n",
    "- Examples: vmware, virtualbox, Amazon EC2\n",
    "\n",
    "## Container\n",
    "\n",
    "- Partially isolated environment that shares the operating system with the host\n",
    "- Same OS kernel as host, but with potentially different users/libraries/networks/etc. and system limits\n",
    "- Examples: Docker, Heroku, Amazon ECS\n",
    "\n",
    "## Virtualenv\n",
    "\n",
    "- Partially isolated **python** environment running in the same OS as the 'host'\n",
    "- **Only** Python packages and environment are isolated:  basically a \"private copy of Python\"\n",
    "- No _security_ isolation from host OS: a program running inside a virtualenv can do whatever a program running outside a virtualenv can do\n",
    "- Similar to local `node_modules` subfolder in a project for Javascript developers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Why Virtualenvs?\n",
    "\n",
    "- **Dependency management** - If two different Python applications require two different versions of the same package, running each app in its own virtualenv allows both versions to be available to their respective applications\n",
    "- **Keeps your system Python pristine** - Many OSes use Python to implement some of the OS tooling (RedHat in particular). This often results in an older version of Python, or particuar versions of Python packages installed globally that you *should not modify* if you want your system tools to keep working.\n",
    "- **Helps with reproducibility** - Virtualenvs allow you to note the versions of all packages installed in your venv in order to recreate the virtualenv on another machine. This prevents the \"Works on My Machine\" certification."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating a virtualenv\n",
    "\n",
    "\n",
    "## Installing virtualenv\n",
    "\n",
    "Since Python 3.3, Python has included a tool to create virtual environments called `venv` in the standard library. \n",
    "\n",
    "If, however, you are developing on Ubuntu, you must separately install it anyway with `apt-get install python-venv`.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating the virtualenv\n",
    "\n",
    "To create a virtual environment, you invoke the `venv` module with the virtualenv name:\n",
    "\n",
    "```shell\n",
    "$ python -m venv env-folder\n",
    "```\n",
    "\n",
    "This command\n",
    "\n",
    "- creates a folder named `env-folder`\n",
    "- copies the Python you used to invoke `venv` into that folder\n",
    "- creates a couple of helper scripts inside env-folder to activate/deactivate the virtualenv"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/Users/rick446/.virtualenvs/py37/bin/python\r\n"
     ]
    }
   ],
   "source": [
    "!which python"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "!python -m venv data/env-folder"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see the directory structure that the virtual environment created with the `tree` command. If you don't have `tree`, you can install it on a Mac using homebrew:\n",
    "\n",
    "```bash\n",
    "$ brew install tree\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "data/env-folder\n",
      "├── bin\n",
      "├── include\n",
      "└── lib\n",
      "    └── python3.7\n",
      "        └── site-packages\n",
      "            ├── __pycache__\n",
      "            ├── pip\n",
      "            │   ├── __pycache__\n",
      "            │   ├── _internal\n",
      "            │   │   ├── __pycache__\n",
      "            │   │   ├── cli\n",
      "            │   │   │   └── __pycache__\n",
      "            │   │   ├── commands\n",
      "            │   │   │   └── __pycache__\n",
      "            │   │   ├── distributions\n",
      "            │   │   │   └── __pycache__\n",
      "            │   │   ├── models\n",
      "            │   │   │   └── __pycache__\n",
      "            │   │   ├── operations\n",
      "            │   │   │   └── __pycache__\n",
      "            │   │   ├── req\n",
      "            │   │   │   └── __pycache__\n",
      "            │   │   ├── utils\n",
      "            │   │   │   └── __pycache__\n",
      "            │   │   └── vcs\n",
      "            │   │       └── __pycache__\n",
      "            │   └── _vendor\n",
      "            │       ├── __pycache__\n",
      "            │       ├── cachecontrol\n",
      "            │       │   ├── __pycache__\n",
      "            │       │   └── caches\n",
      "            │       │       └── __pycache__\n",
      "            │       ├── certifi\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── chardet\n",
      "            │       │   ├── __pycache__\n",
      "            │       │   └── cli\n",
      "            │       │       └── __pycache__\n",
      "            │       ├── colorama\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── distlib\n",
      "            │       │   ├── __pycache__\n",
      "            │       │   └── _backport\n",
      "            │       │       └── __pycache__\n",
      "            │       ├── html5lib\n",
      "            │       │   ├── __pycache__\n",
      "            │       │   ├── _trie\n",
      "            │       │   │   └── __pycache__\n",
      "            │       │   ├── filters\n",
      "            │       │   │   └── __pycache__\n",
      "            │       │   ├── treeadapters\n",
      "            │       │   │   └── __pycache__\n",
      "            │       │   ├── treebuilders\n",
      "            │       │   │   └── __pycache__\n",
      "            │       │   └── treewalkers\n",
      "            │       │       └── __pycache__\n",
      "            │       ├── idna\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── lockfile\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── msgpack\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── packaging\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── pep517\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── pkg_resources\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── progress\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── pytoml\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── requests\n",
      "            │       │   └── __pycache__\n",
      "            │       ├── urllib3\n",
      "            │       │   ├── __pycache__\n",
      "            │       │   ├── contrib\n",
      "            │       │   │   ├── __pycache__\n",
      "            │       │   │   └── _securetransport\n",
      "            │       │   │       └── __pycache__\n",
      "            │       │   ├── packages\n",
      "            │       │   │   ├── __pycache__\n",
      "            │       │   │   ├── backports\n",
      "            │       │   │   │   └── __pycache__\n",
      "            │       │   │   ├── rfc3986\n",
      "            │       │   │   │   └── __pycache__\n",
      "            │       │   │   └── ssl_match_hostname\n",
      "            │       │   │       └── __pycache__\n",
      "            │       │   └── util\n",
      "            │       │       └── __pycache__\n",
      "            │       └── webencodings\n",
      "            │           └── __pycache__\n",
      "            ├── pip-19.2.3.dist-info\n",
      "            ├── pkg_resources\n",
      "            │   ├── __pycache__\n",
      "            │   ├── _vendor\n",
      "            │   │   ├── __pycache__\n",
      "            │   │   └── packaging\n",
      "            │   │       └── __pycache__\n",
      "            │   └── extern\n",
      "            │       └── __pycache__\n",
      "            ├── setuptools\n",
      "            │   ├── __pycache__\n",
      "            │   ├── _vendor\n",
      "            │   │   ├── __pycache__\n",
      "            │   │   └── packaging\n",
      "            │   │       └── __pycache__\n",
      "            │   ├── command\n",
      "            │   │   └── __pycache__\n",
      "            │   └── extern\n",
      "            │       └── __pycache__\n",
      "            └── setuptools-41.2.0.dist-info\n",
      "\n",
      "112 directories\n"
     ]
    }
   ],
   "source": [
    "!tree -d data/env-folder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "total 64\r\n",
      "-rw-r--r--  1 rick446  staff  2247 Jun 30 09:15 activate\r\n",
      "-rw-r--r--  1 rick446  staff  1299 Jun 30 09:15 activate.csh\r\n",
      "-rw-r--r--  1 rick446  staff  2451 Jun 30 09:15 activate.fish\r\n",
      "-rwxr-xr-x  1 rick446  staff   279 Jun 30 09:15 \u001b[31measy_install\u001b[m\u001b[m\r\n",
      "-rwxr-xr-x  1 rick446  staff   279 Jun 30 09:15 \u001b[31measy_install-3.7\u001b[m\u001b[m\r\n",
      "-rwxr-xr-x  1 rick446  staff   261 Jun 30 09:15 \u001b[31mpip\u001b[m\u001b[m\r\n",
      "-rwxr-xr-x  1 rick446  staff   261 Jun 30 09:15 \u001b[31mpip3\u001b[m\u001b[m\r\n",
      "-rwxr-xr-x  1 rick446  staff   261 Jun 30 09:15 \u001b[31mpip3.7\u001b[m\u001b[m\r\n",
      "lrwxr-xr-x  1 rick446  staff    43 Jun 30 09:15 \u001b[35mpython\u001b[m\u001b[m -> /Users/rick446/.virtualenvs/py37/bin/python\r\n",
      "lrwxr-xr-x  1 rick446  staff     6 Jun 30 09:15 \u001b[35mpython3\u001b[m\u001b[m -> python\r\n"
     ]
    }
   ],
   "source": [
    "!ls -l data/env-folder/bin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### (windows note)\n",
    "\n",
    "If you are using Windows, there should be a `Scripts` folder under the environment folder instead of `bin`, and it should contain an `activate.bat` file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can invoke the Python in your new virtualenv by specifying the full path:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Python 3.7.6\r\n"
     ]
    }
   ],
   "source": [
    "!data/env-folder/bin/python --version"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/Users/rick446/src/arborian-classes/data/env-folder/bin/python\r\n"
     ]
    }
   ],
   "source": [
    "!data/env-folder/bin/python -c 'import sys; print(sys.executable)'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['', '/usr/local/Cellar/python/3.7.6_1/Frameworks/Python.framework/Versions/3.7/lib/python37.zip', '/usr/local/Cellar/python/3.7.6_1/Frameworks/Python.framework/Versions/3.7/lib/python3.7', '/usr/local/Cellar/python/3.7.6_1/Frameworks/Python.framework/Versions/3.7/lib/python3.7/lib-dynload', '/Users/rick446/src/arborian-classes/data/env-folder/lib/python3.7/site-packages']\r\n"
     ]
    }
   ],
   "source": [
    "!data/env-folder/bin/python -c 'import sys; print(sys.path)'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Activating virtual environments\n",
    "\n",
    "More commonly, we will *activate* the virtualenv for our current shell by `source`-ing the `activate` script\n",
    "\n",
    "### Linux\n",
    "\n",
    "```shell\n",
    "$ source env-folder/bin/activate\n",
    "(env-folder) $\n",
    "```\n",
    "\n",
    "or\n",
    "\n",
    "```shell\n",
    "$ . env-folder/bin/activate\n",
    "(env-folder) $\n",
    "```\n",
    "\n",
    "### Windows\n",
    "\n",
    "```shell\n",
    "c:\\...> env-folder\\Scripts\\activate.bat\n",
    "(env-folder) c:\\...>\n",
    "```\n",
    "\n",
    "Activating the virtualenv does a few things to your *current shell/terminal window only*:\n",
    "\n",
    "- Puts the virtualenv's executable folder (`bin` or `Scripts`) at the beginning of your path so the virtualenv python will be picked up automatically\n",
    "- Changes your prompt so you see that you are in the virtualenv\n",
    "- Makes a `deactivate` command available to undo the changes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deactivating virtual environments\n",
    "\n",
    "### Linux\n",
    "\n",
    "```shell\n",
    "(env-folder) $ deactivate\n",
    "$\n",
    "```\n",
    "\n",
    "### Windows\n",
    "\n",
    "```shell\n",
    "(env-folder) c:\\...> deactivate\n",
    "c:\\...> \n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ACTIVATE\n",
      "/Users/rick446/src/arborian-classes/src/data/env-folder/bin/python\n",
      "My prompt is now (env-folder)\n",
      "/Users/rick446/src/arborian-classes/data/env-folder/bin/python\n",
      "DEACTIVATE\n",
      "/Users/rick446/.virtualenvs/py37/bin/python\n",
      "/Users/rick446/.virtualenvs/py37/bin/python\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "echo \"ACTIVATE\"\n",
    "source data/env-folder/bin/activate\n",
    "which python\n",
    "echo My prompt is now $PS1\n",
    "python -c 'import sys; print(sys.executable)'\n",
    "echo \"DEACTIVATE\"\n",
    "deactivate\n",
    "which python\n",
    "python -c 'import sys; print(sys.executable)'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Installing packages in virtual environments\n",
    "\n",
    "When the virtual environment is activated, or when you invoke the version of Python in the virtualenv, you can install third-party packages into the virtualenv without modifying your system Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/Users/rick446/src/arborian-classes/src/data/env-folder/bin/python\n",
      "Looking in links: /Users/rick446/src/wheelhouse\n",
      "Requirement already up-to-date: pip in /Users/rick446/src/arborian-classes/data/env-folder/lib/python3.7/site-packages (20.1.1)\n",
      "Looking in links: /Users/rick446/src/wheelhouse\n",
      "Requirement already satisfied: numpy in /Users/rick446/src/arborian-classes/data/env-folder/lib/python3.7/site-packages (1.19.0)\n",
      "<module 'numpy' from '/Users/rick446/src/arborian-classes/data/env-folder/lib/python3.7/site-packages/numpy/__init__.py'>\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "set -e\n",
    "source data/env-folder/bin/activate\n",
    "which python\n",
    "pip install -U pip\n",
    "pip install numpy\n",
    "python -c 'import numpy; print(numpy)'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using requirements.txt for reproducibility\n",
    "\n",
    "Once you have your app in your virtualenv running, you may need to reproduce the virtualenv on another machine. \n",
    "`pip` has a command `freeze` which outputs the exact versions of all packages installed in a virtualenv:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "set -e\n",
    "source data/env-folder/bin/activate\n",
    "python -m pip freeze"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Normally, we'll put this into a file `requirements.txt` that we check into source control and distribute with our project:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "set -e\n",
    "source data/env-folder/bin/activate\n",
    "pip freeze > data/requirements.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have the requirements.txt file, we can create a new virtualenv and install all the same versions of packages into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "set -e\n",
    "python -m venv data/env-folder-2\n",
    "source data/env-folder-2/bin/activate\n",
    "python -m pip install -r data/requirements.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using wheels for faster installations\n",
    "\n",
    "While `pip` tries to cache as much data as possible, we can do even better by using \"wheels.\" \n",
    "\n",
    "Wheels are Python packages that have been compiled (if necessary) for a particular target architecture and are thus much faster to install. \n",
    "\n",
    "If you're moving to a new machine (for instance, when deploying to production) it can also be useful to have the wheels cached locally so `pip` doesn't try to download the packages from the Python Package Index."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "set -e\n",
    "source data/env-folder/bin/activate\n",
    "pip install scipy scikit-learn jupyter simplejson pymongo boto3 wheel\n",
    "pip freeze > data/requirements.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "cat data/requirements.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "set -e\n",
    "source data/env-folder/bin/activate\n",
    "pip wheel -w data/wheelhouse -r data/requirements.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "ls data/wheelhouse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can distribute the `data/wheelhouse` directory with our project and install everything from the wheelhouse and not fetch from PyPI:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "set -e\n",
    "source data/env-folder-2/bin/activate\n",
    "pip install --no-index -f data/wheelhouse -r data/requirements.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cleaning up virtualenvs\n",
    "\n",
    "Since a virtualenv is just a directory, we can 'clean it up' by removing the directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!rm -r data/env-folder data/env-folder-2 data/wheelhouse data/requirements.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!python -m venv --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab\n",
    "\n",
    "Open [virtualenv lab][virtualenv-lab]\n",
    "\n",
    "[virtualenv-lab]: ./virtualenv-lab.ipynb"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
