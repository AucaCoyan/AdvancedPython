def divisible_by_3(num: int) -> bool:
    if num % 3 == 0:
        return True
    elif num % 3 != 0:
        return False


def divisible_by_5(num: int) -> bool:
    if num % 5 == 0:
        return True
    elif num % 5 != 0:
        return False


for i in range(100):
    if divisible_by_3(i) and divisible_by_5(i):
        print(f'{i} foobar')
    elif divisible_by_3(i):
        print(f'{i} foo')
    elif divisible_by_5(i):
        print(f'{i} bar')
    else:
        print(f'{i} is not foo or bar')